
TABLE CREATION -

CREATE TABLE salespeople ( snum int(4), sname varchar(10), city varchar(10), comm float(3,2) );

CREATE TABLE customers ( cnum int(4), cname varchar(10), city varchar(10), rating int(4), snum int(4) );

CREATE TABLES orders (Onum int(4) ,Amt  float(7,2) ,Odate date ,Cnum  int(4)  ,Snum  int(4));


mysql> show tables;
+---------------------+
| Tables_in_classwork |
+---------------------+
| bonus               |
| books               |
| customers           |
| dept                |
| dummy               |
| emp                 |
| mobiles             |
| newemp              |
| operatingsystems    |
| orders              |
| salespeople         |
| salgrade            |
| stud                |
| temp                |
+---------------------+
14 rows in set (0.01 sec)



DATA INSERTED


Insert into ORDERS(Onum,Amt,Odate,Cnum,Snum)
values
('3001','150.5','2022-02-21','2005','1002'),
('3002','270.65','2022-01-10','2001','1005'),
('3003','65.26','2022-02-22','2002','1001'),
('3004','110.5','2022-02-25','2004','1003'),
('3005','948.5','2022-02-28','2003','1004'),
('3006','2400.6','2022-01-30','2001','1001');


Insert into CUSTOMERS
values
('2001','Nick Rima','New York','100','1001');
('2002','Brad Guzan','California','200','1002');
('2003','Graham Zusi','Londan','300','1005');
('2004','Brad Davis','Berlin','200','1003');
('2005','Jozy Green','Londan','300','1004');


Insert into SALESPEOPLE(Snum,Sname,City,Comm)
values
('1001','James Clay','Mumbai','0.15'),
('1002', 'Pit Knite','Pune','0.13'),
('1003','Nail Alex','Londan','0.11'),
('1004','Mc Lyon','Paris','0.12'),
('1005','Adan Paul','Goa','0.13');



1. Write a query that will give you all orders for more than Rs. 1,000. 
   
mysql> select * from ORDERS where Amt > 1000;

+------+---------+------------+------+------+
| Onum | Amt     | Odate      | Cnum | Snum |
+------+---------+------------+------+------+
| 3006 | 2400.60 | 2022-01-30 | 2001 | 1001 |
+------+---------+------------+------+------+
1 row in set (0.01 sec)


2. Write a query that selects each customer’s smallest order.
mysql> select min(Amt),Cnum
    -> from ORDERS
    -> Group By Cnum;
+----------+------+
| min(Amt) | Cnum |
+----------+------+
|   150.50 | 2005 |
|   270.65 | 2001 |
|    65.26 | 2002 |
|   110.50 | 2004 |
|   948.50 | 2003 |
+----------+------+
5 rows in set (0.01 sec)



3. Write a query that totals the orders for each day and places the results in descending order.

mysql> SELECT sname,
    ->        snum,
    ->        cname
    -> FROM salespeople
    -> JOIN customers using(snum)
    -> WHERE snum IN
    ->     (SELECT snum
    ->      FROM customers
    ->      GROUP BY snum
    ->      HAVING count(snum)>1)
    -> ORDER BY sname,
    ->          cname;
+--------+------+------------+
| sname  | snum | cname      |
+--------+------+------------+
| serres | 1002 | Brad Guzan |
| serres | 1002 | Brad Guzan |
+--------+------+------------+

4. Write a command that produces the name and number of each salesperson and each customer with more than one current order. Put the results in alphabetical order.

mysql> SELECT sname,
    ->        snum,
    ->        cname
    -> FROM salespeople
    -> JOIN customers using(snum)
    -> WHERE snum IN
    ->     (SELECT snum
    ->      FROM customers
    ->      GROUP BY snum
    ->      HAVING count(snum)>1)
    -> ORDER BY sname,
    ->          cname;
+--------+------+------------+
| sname  | snum | cname      |
+--------+------+------------+
| serres | 1002 | Brad Guzan |
| serres | 1002 | Brad Guzan |
+--------+------+------------+
2 rows in set (0.00 sec)

5. Write a query that selects all orders for amounts greater than any for the customers in London.

mysql> SELECT *
    -> FROM orders
    -> WHERE amt> ANY
    ->     ( SELECT amt
    ->      FROM orders
    ->      WHERE cnum IN
    ->          ( SELECT cnum
    ->           FROM customers
    ->           WHERE city='london' ) );
Empty set (0.01 sec)



6. Show the date after 5 days from today.

mysql> select * from orders where odate > current_date + interval 5 day;
Empty set (0.01 sec)

7. Show the format of date in the following form 

    Friday 23rd June 2017 11:59:30 PM


SELECT FORMAT (getdate(), 'dddd, MMMM, yyyy hh:mm:ss tt') as date




Store Function



store function

CREATE TABLE tempp ( item number, square number, CUBE number );

 TABLE created.

DECLARE num number:=&num;

 BEGIN
INSERT INTO tempp
VALUES(num,
       num*num,
       num*num*num);

 END;

 Enter value
FOR num: 5 --PL/SQL procedure successfully completed.

SELECT *
FROM tempp;

 --ITEM  SQUARE  CUBE
--5   25   125